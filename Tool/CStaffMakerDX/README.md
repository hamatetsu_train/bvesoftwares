# CStaffMakerDX / Ex  
ver 1.1.2  
BVE Trainsim 6 / 5.8向けの車両拡張プラグインです。  
3の会社のスタフっぽい画像をパネルにはめ込みます。  

  
## 構成  
※x64フォルダはBVE Trainsim 6向け、x86フォルダはBVE Trainsim 5.8向けです。  
・ CStaffMakerDX.dll		…プラグイン本体  
・ CStaffMakerDX.xml		…プラグイン設定  
※以下の2つはzbx1425さんの[DXDynamicTexture](https://github.com/zbx1425/DXDynamicTexture)関連です。  
・ Harmony-net48.dll / Harmony-net35.dll  
・ Zbx1425.DXDynamicTexture-net48.dll / Zbx1425.DXDynamicTexture-net35.dll  
  
※ExフォルダはAtsEx利用版のCSTaffMakerExです。
CStaffMakerExの[README](https://gitlab.com/hamatetsu_train/bvesoftwares/-/blob/main/Tool/CStaffMakerDX/Ex/)を参照してください。  

  
## 使い方  
### パネル  
スタフの画面を含む画像をPilotLampとして貼ります。  
CStaffMakerはその画像の左上から長方形で描画します。  
画面を傾けたい場合はTiltで調整してください。(PilotLampにはTileがないので、Needleで代用してください。)  
※Tiltを使わずに自由変形させる機能も内蔵していますが、若干重いので、試したい方は別途お問い合わせください。  
  
### 設定ファイル  
GENERAL - Size : スタフのサイズをwidth,heightの形で記述します。  
GENERAL - ImageName : スタフ画像に置換する画像のファイル名を記述します。(停車場リストが選択されなかった場合に表示されます。)  
GENERAL - Magnet : スタフの上に被せるマグネットの画像のパス(プラグインからの相対パス)を記述します。(300*100px)  
KEYASSIGN - Up : マグネットを上に動かすキーを指定します。  
KEYASSIGN - Down : マグネットを下に動かすキーを指定します。  
  
  
## 使用
シナリオ読み込み中にファイルダイアログが表示されるので、シナリオファイルまたは停車場リストを選択します。  
※CStaffMakerExについては、AtsEx経由で停車場リストを取得するのでファイルダイアログは表示されません。  
停車場リストについては、[BveEditor](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Tool/BveEditor)の停車場リスト編集機能でCStaffMaker用パラメータを付加した停車場リストにしておくのをおすすめします。  
  
  
## 利用について
▼ダウンロードについて  
　右上の↓マークを押し、ZIPを押してください。  
  
▼プラグインの利用について  
　[ガイドライン](https://annakaworks.sakura.ne.jp/20bvets/guideline.html)にしたがって利用することができます。  
  
▼プラグインを利用したデータの公開について  
　データに同梱のReadMe等に出展を明記した上で、運転可能な車両データと同梱する場合に限り、車両データ作者様による当プラグインの配布を認めます。  
　作者名はハルピー、ＵＲＬは[https://annakaworks.sakura.ne.jp/](https://annakaworks.sakura.ne.jp/)でお願いします。  
  
  
## こんなときは
▼「次のアプリケーションでは、Microsoft .NET Framework 4.8 またはそれ以降が必要です」のエラーが出る場合  
　このプラグインはBVE Trainsim 6と同じく.NET Flamework 4.8 向けで作成しています。そのため、.NET Flamework4.8がインストールされていない場合、上記のようなエラーが出る可能性があります。その場合は Windows Update 等で最新の.Net Flamework をインストールしてください。  
※Microsoft 以外のサイトからダウンロードしてインストールしたことにより損害が発生しても一切責任を負いません。自己責任でお願いします。  
※Windows 10 May 2019 Update (1903)以降のWindowsであれば入っているはず。  
  
▼CStaffMakerDxをBVE5で使用したときに「DXDynamicTextureが見つからない」のエラーが出る場合  
　AtsExを使用するプラグインと併用する場合、x64フォルダにあるHarmony-net48.dllとZbx1425.DXDynamicTexture-net48.dllをx86フォルダにコピーしてください。  
　※AtsExが動作するように対象フレームワークを4.0に変えるため、CStaffMakerDxは上記2ファイルが必要になってしまいます。  

▼その他のエラーが出た場合  
　プラグインのバージョン、エラーの内容、エラーが出た時の状況、プラグイン設定ファイルの内容等を詳しく教えてください。  
　また、プラグインと同じフォルダにログファイルを出力していますので、そちらをお送りいただく場合があります。  
　「エラーが起きました！」とかBVEのエラーリストの羅列を送りつけてくるだけでは解決ができません。  
　また、エゴサしているわけでもありませんので、直接ご報告ください。  
  
  
