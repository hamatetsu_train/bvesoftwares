﻿# CASTもどき
BVE Trainsim 6(一部はBVE5.8も)向けのATSプラグインです。

※2024/06/28 AtsEx ver1.0-rc9のマップステートメントを使用した路線データでは正常に動作する保証はしません。  
※2024/10/06 AtsEx ver1.0-rc10の変数や計算式を含んだinclude文や、ex_relativedirを使用した路線データに暫定対応しました。  
※2025/01/02 BveExに暫定対応しました。CastPanel2のBveEx対応は行いません。あらかじめご了承ください。(CastPanelDXはAtsEX不使用なのでBveEX環境でも動作すると思われますが、サポート対象外となります。)  
※2024/02/08 BveEx ver2.0.5以降に追加されたマップステートメントを使用した路線データでは正常に動作する保証はしません。  

## 内容
・旧型CASTもどき(CastPanelDX/CastPanelDX.dll, CastPanelEX/CastPanelEX.dll, Program/CastPanel2.exe)  
・新型CASTもどき(CastPanelEX3/CastPanelEX3.dll, Program3/CastPanel3.exe)  
　※CastPanelEX3T/CastPanelEX3T.dllはBVE画面内タッチ対応版です。  
・旧CASTもどきデータ変換ツール(CPConverter.exe)  
  
  
## CASTもどき
### CastPanel3
BVE Trainsimからの新型CASTもどきへのデータの送信またはパネルファイルへの埋め込みはCastPanelEX3.dll/CastPanelEX3T.dllを車両データに組み込んでください。  
CastPanelEX3.dllは(車両ファイル名).VehiclePluginUsing.xmlにCastPanelEX3.dllを指定してください。  
※CastPanelEX3の両数は、実際に使用する車両データの両数を表示します。CastPanel3の両数は、停車場リストファイルに設定された両数を表示します。  

※マニュアルに未掲載の内容  
・発車2分前に鳴るサウンドと停車駅に接近した際に鳴るサウンドを設定できます。  
　CastPanel3の場合は、exeと同じフォルダにdeparture.wavとnear.wavを置いてください。  
　CastPanelEX3の場合は、車両データのSoundファイルで指定し、CastPanelEX3.xmlにサウンドインデックスを指定してください。  
　※サンプルのdeparture.wavとnear.wavを添付しています。ただし、departure.wavについては車両データと一緒であっても配布不可としますので別途ご用意ください。 
  
・CastPanelEX3Tは設定ファイルにてパネル上のレイヤー番号と設置座標が必要です。  
  CAST→Locationに座標、CAST→Layerにレイヤー番号を記載してください。  
  
・CastPanelEX3Tは「次へ」「戻る」ボタンは動作します。逆に、押さないと自動では進みません。  
  なお、運転台の揺れやCastオブジェクトのTiltには対応していません。  
  

### CastPanel2
BVE TrainsimからのCAST用走行データの送信またはパネルファイルへの埋め込みはCastもどきプラグインCastPanelDXかCastPanelEXを車両データに組み込んでください。  
なお、データはBVE側のCastPanelDX/CastPanelEXプラグインから送信されるようになり、旧CASTもどき用のデータは使用しなくなります。旧CASTもどきデータ変換ツールを使用することにより、BVE Trainsimの路線データに組み込みできるファイルができますので、コピペするなりincludeするなりしてください。  
  
  
## ドキュメント
　[CASTもどきマニュアル](https://drive.google.com/file/d/1GiIRDog_FP2wT7zD9yahDa0fsUU_irVa/view?usp=sharing)  
　[CASTもどき3マニュアル](https://drive.google.com/file/d/1x1qoRlyQ8SvxoerbExSDGVzM5gq7dKAQ/view?usp=sharing)  
※基本的に2と3で設定の仕様は変わりません。3で自車全車の表示を行う場合は、最新版のCastPanel対応停車場リストが必要です。(2用だと全車は表示されません) 最新版のCastPanel対応停車場リストは2に対応していません。      
  
## 利用について
▼ダウンロードについて  
　右上の↓マークを押し、ZIPを押してください。  
  
▼利用について  
　[ガイドライン](https://annakaworks.sakura.ne.jp/20bvets/guideline.html)にしたがって利用することができます。  
　※ただし、departure.wavについては車両データと一緒であっても配布不可としますので別途ご用意ください。 
  
▼プラグインを利用したデータの公開について  
　データに同梱のReadMe等に出展を明記した上で、運転可能な車両データと同梱する場合に限り、車両データ作者様による当プラグインの配布を認めます。  
　作者名はハルピー、ＵＲＬは[https://annakaworks.sakura.ne.jp/](https://annakaworks.sakura.ne.jp/)でお願いします。  
  
  
## こんなときは
▼ Windows8.1以前、Windows以外で正常動作しない  
  MicrosoftがサポートしているWindows11、Windows10以外の環境での利用はサポート対象外です。  
  またWindows11であってもArm版はサポート対象外です。  

▼「次のアプリケーションでは、Microsoft .NET Framework 4.8 またはそれ以降が必要です」のエラーが出る場合  
　CASTもどきはBVE6と同じく.NET Flamework 4.8 向けで作成しています。そのため、.NET Flamework4.8がインストールされていない場合、上記のようなエラーが出る可能性があります。その場合は Windows Update 等で最新の.Net Flamework をインストールしてください。  
※Microsoft 以外のサイトからダウンロードしてインストールしたことにより損害が発生しても一切責任を負いません。自己責任でお願いします。  
※Windows 10 May 2019 Update (1903)以降のWindowsであれば入っているはず。  
  
▼その他のエラーが出た場合  
　エラーの内容、エラーが出た時の状況、操作、設定内容等を詳しく教えてください。  
　「エラーが起きました！」とかBVE Trainsimのエラーリストの羅列を送りつけてくるだけでは解決ができません。  
　また、エゴサしているわけでもありませんので、直接ご報告ください。  
  
  
## ライセンス
・サンプルのdeparture.wavは「VOICEROID2 音街ウナ」を使用しています。  