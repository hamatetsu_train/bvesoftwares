# 名鉄タブレット時刻表もどきプラグイン
BVE Trainsim 6 / 5.8向けのATSプラグイン/アプリケーションです。  
  
## Known Isuues
・(MStaffEX) AtsEX ver1.0-rc9のマップステートメントを使用した路線データは正常に動作する保証はありません。    
・(MStaffEX) AtsEX ver1.0-rc10の変数や計算式を含んだinclude文や、ex_relativedirを使用した路線データに暫定対応しました。    
・(MStaffEX) ファイルサイズの大きいシナリオを起動しようとすると起動するまでに時間がかかる。  
　→シナリオの解析方法を変更しました。(全読み込みではなくStation.Loadが出てきた時点で解析を終了する)    

  
## 内容
・名鉄タブレット時刻表もどきプラグイン(MStaffEX)  
・名鉄タブレット時刻表もどきアプリケーション(MStaff)  
  
  
## 車両データに組み込む
### ダウンロード  
[MStaffEX](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Tool/MStaff/MStaffEX)

MStaffEXはAtsEXの車両プラグインとして動作します。
[AtsEX](https://automatic9045.github.io/contents/bve/AtsEX/)のATSプラグイン版が別途必要です。  
AtsEXのサンプル車両データからAtsEx.x～.Caller.dllをコピーし、AtsEx.x～.Caller.dllからMStaffEX.dllが読み込まれるようにしてください。  
※本プラグインはTickResultに影響を与えません。  

### 設定  
MStaffEX.dllと同じフォルダにMStaffEX.jsonを配置してください。  
※[設定方法はこちら](https://gitlab.com/hamatetsu_train/bvesoftwares/-/blob/main/Tool/MStaff/AboutConfig.md)

#### パネル  
名鉄タブレット時刻表の画面を含む画像をPilotLampとして貼ります。  
MStaffEXはその画像の左上から長方形で描画します。画面を傾けたい場合はNeddleとして貼ってTiltで調整してください。(プラグインに変形機能があるにはありますが、めちゃくちゃ重いので推奨しません)  
  
#### 外部送信  
ネットワークを通じてMStaffにデータを送信します。  
ファイアウォールもしくはお使いのセキュリティソフトにて送信の許可が必要になる場合があります。  


## 路線データに組み込む
### 地上子  

|Type|種類|Optional|
|----|----|----|
|85000|初期化|0|
  
・85000  
画面や内部データを初期状態にします。MStaffEX用地上子の中で一番小さい距離程に設置してください。設置しないと、シナリオがリロードされた場合にリロード前の表示が残る可能性があります。  
  
  
設置例  
0;  
Beacon.Put(85000, 1, 0);  
  
  
### 停車場リスト  
停車場リストについては、[BveEditor](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Tool/BveEditor)の停車場リスト編集機能でMStaff用パラメータを付加した停車場リストにしてください。  

  
## MStaff
画面をダブルクリックすると、最大化するのと同時に設定画面が表示されます。  
リッスンしたいポート番号を設定して開始ボタンを押してください。  
再度画面をダブルクリックすると、画面サイズが元に戻ります。  
終了する場合は、もとのサイズに戻した上で右上のバツボタンを押してください。  

利用する場合には、MStaff用のデータを送信する車両データを動作しているBveTrainsimをプレイしている端末と同じ端末内もしくは同じネットワーク内にあること。  


## ソースについて
[ソース](https://gitlab.com/hamatetsu_train/mstaff)  
AtsEXのSDKが別途必要です。
MStaffEXの参照設定にて、AtsEX、AtsEx.PluginHost、BveTypesの各プロジェクトを参照していますので、ソース利用時は参照設定を修正してください。  
NuGet等に無いDLLファイルについてはDLLフォルダに入れてあります。  


## 利用について
▼ダウンロードについて  
　右上の↓マークを押し、ZIPを押してください。  
  
▼プラグインの利用について  
　[ガイドライン](https://annakaworks.sakura.ne.jp/20bvets/guideline.html)にしたがって利用することができます。  
  
▼プラグインを利用したデータの公開について  
　データに同梱のReadMe等に出展を明記した上で、運転可能な車両データと同梱する場合に限り、車両データ作者様による当プラグインの配布を認めます。  
　作者名はハルピー、ＵＲＬは[https://annakaworks.sakura.ne.jp/](https://annakaworks.sakura.ne.jp/)でお願いします。  
  
  
## こんなときは
▼「次のアプリケーションでは、Microsoft .NET Framework 4.8 またはそれ以降が必要です」のエラーが出る場合  
　このプラグインはBVE Trainsim 6と同じく.NET Flamework 4.8 向けで作成しています。そのため、.NET Flamework4.8がインストールされていない場合、上記のようなエラーが出る可能性があります。その場合は Windows Update 等で最新の.Net Flamework をインストールしてください。  
※Microsoft 以外のサイトからダウンロードしてインストールしたことにより損害が発生しても一切責任を負いません。自己責任でお願いします。  
※Windows 10 May 2019 Update (1903)以降のWindowsであれば入っているはず。  
  
▼BVE5で使用したときに「DXDynamicTextureが見つからない」のエラーが出る場合  
　AtsExを使用するプラグインと併用する場合、x64フォルダにあるHarmony-net48.dllとZbx1425.DXDynamicTexture-net48.dllをx86フォルダにコピーしてください。  
　※AtsExが動作するように対象フレームワークを4.0に変えるため、MStaffEXは上記2ファイルが必要になってしまいます。  

▼その他のエラーが出た場合  
　プラグインのバージョン、エラーの内容、エラーが出た時の状況、プラグイン設定ファイルの内容等を詳しく教えてください。  
　また、プラグインと同じフォルダにログファイルを出力していますので、そちらをお送りいただく場合があります。  
　「エラーが起きました！」とかBVEのエラーリストの羅列を送りつけてくるだけでは解決ができません。  
　また、エゴサしているわけでもありませんので、直接ご報告ください。  
  
  
