# 名鉄タブレット時刻表もどきプラグイン
Bve Trainsim 6 / 5.8向けのATSプラグイン/アプリケーションです。  
  
  
## (新) JSON形式
MStaffEX.dllと同じフォルダにMStaffEX.jsonを配置してください。  
  
### 設定項目
MStaff - Size : 名鉄タブレット時刻表画面のサイズをwidth,heightの形で記述します。(できればW:H=3:4)  
MStaff - ImageName : 名鉄タブレット時刻表画面を含む画像のファイル名を記述します。  
MStaff - Deformation : true/false。trueの場合プラグイン側で名鉄タブレット時刻表画面を変形します。  
MStaff - DeformationPoint - TopLeft : 変形前の左上の頂点の移動後の座標をx,yの形で記述します。  
MStaff - DeformationPoint - TopRight : 変形前の右上の頂点の移動後の座標をx,yの形で記述します。  
MStaff - DeformationPoint - BottomLeft : 変形前の左下の頂点の移動後の座標をx,yの形で記述します。  
MStaff - DeformationPoint - BottomRight : 変形前の右下の頂点の移動後の座標をx,yの形で記述します。  
Com - UseCom : 外部送信を有効にする場合はtrue。  
Com - Output : ※true固定です。  
Com - HostName : 送信先の端末名もしくはIPアドレス。自端末の場合はlocalhostまたは127.0.0.1。(端末名にした場合、名前解決に時間がかかってカクつく場合があります)  
Com - SendPort : 名鉄タブレット時刻表もどきアプリが待ち受けているポート番号。(1024～65535)  
  
  
## (旧) XML形式
MStaffEX.dllと同じフォルダにMStaffEX.xmlを配置してください。  
MStaffEX.dllと同じフォルダにMStaffEX.jsonがある場合、jsonのほうが優先されます。  
  
### 設定項目
CAST - Size : 名鉄タブレット時刻表画面のサイズをwidth,heightの形で記述します。(できればW:H=3:4)  
CAST - ImageName : 名鉄タブレット時刻表画面を含む画像のファイル名を記述します。  
CAST - UseCom : 外部送信を有効にする場合はtrue。  
CAST - Output : ※true固定です。  
CAST - HostName : 送信先の端末名もしくはIPアドレス。自端末の場合はlocalhostまたは127.0.0.1。  
CAST - SendPort : 名鉄タブレット時刻表もどきアプリが待ち受けているポート番号。(1024～65535)  
  

