# Hamatetsu's Bve Softwares - HROneman
BVE Trainsim 6 / 5.8向けのATSプラグインです。  
全車・自車、自動・半自動に対応したワンマン運転プラグインです。  
  
BveEXプラグインになりますので、BveEXが別途必要です。  
  
## ドキュメント
・必要なファイル  
　・HROneman.dll：本体 (ver.1.2.1)  
　・HROneman.json：設定ファイル  
　・Microsoft.Bcl.AsyncInterfaces.dll：HROneman.dllと同じフォルダに配置。※製品バージョン：8.0.23.53103  
　・System.Text.Encodings.Web.dll：HROneman.dllと同じフォルダに配置。※製品バージョン：8.0.23.53103  
　・System.Text.Json.dll：HROneman.dllと同じフォルダに配置。※製品バージョン：8.0.1024.46610  
　　※これら3つを他のプラグインも使用する場合は、バージョンを合わせてください。    
※別途BveEXが必要です。　対応バージョン：2.0.0
　本プラグインはatsHandleの値に影響を与えません。  
※DLしたzipファイルは、右クリック→プロパティ→「ブロック解除」にチェックを入れてOK→解凍 してください。  
　それをやらないと使用時に0x80131515エラーが発生します。  
  
  
・設定方法  
設定はjson形式の設定ファイルで行います。ファイル名はプラグインのファイル名と同じにしてください。  
※HROneman.dllに対しての設定ファイルはHROneman.json  
※配布する際は４つのDLLをすべて添付してください。～.VehiclePluginUsing.xmlへの指定はHROneman.dllのみ行ってください。  
  
  
├ [ShowDebugLabel] (false) … 画面の左上に現在の状態についての補助表示を表示するか？。  
├ [Route] … 路線側の設定です。  
｜　└　[Beacons] … 地上子番号の設定   
｜　　　└　[ChangeEnabledBeacon] (-1) … ワンマン・ツーマンを切り替える。(Optionalが0のときはツーマン、それ以外はワンマン)   
└ [Vehicle] … 変換スクリプトを記述します。  
　　├ [IsEnabledByDefault] (false) … シナリオ起動直後にワンマンモードになっていてほしいなら true  
　　├ [KeyAssign] … キー設定  
　　｜　├ [LeftOpen] () … 左のドアを開く(半自動の場合は解錠する)  
　　｜　├ [LeftClose] () … 左のドアを閉める  
　　｜　├ [LeftReopen] () … 左のドアを再開扉する  
　　｜　├ [RightOpen] () … 右のドアを開く(半自動の場合は解錠する)  
　　｜　├ [RightClose] () … 右のドアを閉める  
　　｜　├ [RightReopen] () … 右のドアを再開扉する  
　　｜　├ [OneDoorSW] () … 全車・自車を切り替える  
　　｜　└ [SemiautoSW] () … 自動・半自動を切り替える  
　　├ [AtsPanelValues] … パネル出力先の設定 (※現状のAtsEXの制約により、他のプラグインと併用する場合、他のプラグインが使用するインデックスの重複設定は避けてください。)  
　　｜　├ [Mode] (-1) … モード  
　　｜　├ [NFB] (-1) … [ワンマン]ランプ (ワンマンの場合に点灯)  
　　｜　├ [Semiauto] (-1) … 自動・半自動 (半自動の場合に点灯)  
　　｜　└ [OneDoor] (-1) … 自車・全車 (全車の場合に点灯)  
　　└ [AtsSounds] … サウンド出力先の設定  
　　　　├ [DoorSwitchOn] (-1) … 開スイッチ操作サウンド  
　　　　├ [DoorSwitchOff] (-1) … 閉スイッチ操作サウンド  
　　　　├ [DoorSemiautoOpen] (-1) … 半自動時解錠後サウンド (「入口の整理券を～」)  
　　　　└ [DoorSemiautoClose] (-1) … 半自動時戸閉後サウンド (運賃箱？の「ﾋﾟｰ」)  
[KeyAssign]の各項目について  
　├ [Input] (0) … キーの入力方法(1:保安装置キー か 0:キーボード) ★2024/3/13リリース版より0と1を入れ替えました。★  
　├ [KeyCode] (0) … キー(保安装置キーの場合:0～15、キーボードの場合:仮想ｷｰｺｰﾄﾞ、使用しない場合:Input=0,KeyCode=0)  
　└ [Memo] () … プラグインでは使用しない自由項目  
※( )内の値は記述しなかった場合の値
  
  
## 利用について
▼ダウンロードについて  
　利用したいプラグイン・ツールのあるフォルダに移動し、右上の↓マークを押し、ZIPを押してください。  
  このページの↓マークを押すと、すべてのプラグイン・ツールをダウンロードします。
  
▼プラグインの利用について  
　[ガイドライン](https://annakaworks.sakura.ne.jp/20bvets/guideline.html)にしたがって利用することができます。  
  
▼プラグインを利用したデータの公開について  
　データに同梱のReadMe等に出展を明記した上で、運転可能な車両データと同梱する場合に限り、車両データ作者様による当プラグインの配布を認めます。  
　作者名はハルピー、ＵＲＬは[https://annakaworks.sakura.ne.jp/](https://annakaworks.sakura.ne.jp/)でお願いします。  
  
  
## Ｑ＆Ａ
▼駅ジャンプするとどうなるか？  
　ワンマン運転の駅では、全扉が閉まった状態になります。  
  
▼ Windows8.1以前、Windows以外で正常動作しない  
  MicrosoftがサポートしているWindows11、Windows10以外の環境での利用はサポート対象外です。  
  またWindows11であってもArm版はサポート対象外です。  

▼その他のエラーが出た場合  
　エラーを吐いたプラグイン名、プラグインのバージョン、エラーの内容、エラーが出た時の状況、操作、プラグイン設定ファイルの内容等を詳しく教えてください。  
　プラグインによっては同じフォルダにログファイルを出力していますので、そちらをお送りいただく場合があります。  
　「エラーが起きました！」とかBVEのエラーリストの羅列を送りつけてくるだけでは解決ができません。  
　また、エゴサしているわけでもありませんので、直接ご報告ください。  
  
  
## 謝辞  
　このプラグインはおーとま様の[都市型ワンマンプラグイン](https://www.okaoka-depot.com/AtsEX.Docs/plugins/city-oneman/)を使用しています。  

※ソースファイル  
　└ [HROneman](https://gitlab.com/hamatetsu_train/hroneman)  
