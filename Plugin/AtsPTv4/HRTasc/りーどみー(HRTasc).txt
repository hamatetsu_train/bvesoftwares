////////////////////////////////////////////////////////////////
//                                                            //
//          浜鉄式TASCplugin for BVE Trainsim(BVE5/6)         //
//                                                            //
////////////////////////////////////////////////////////////////

HRTasc.dll　ver 1.2.5 2022/04/29
          　ver 1.0.1 2019/06/30

▼ このフォルダの中身
・HRTasc.dll			… プラグイン本体
・HRTasc.xml			… 設定ファイル
・HRTasc.pdf			… 説明書
・りーどみー(HRTasc).txt	… このファイル


　多段階ブレーキTASC、あおなみ線式TASCを再現するプラグインです。
　ホームドア機能も付きました。
　PluginConverterで他のプラグインと組み合わせてご利用ください。


(c)ハルピー  Twitter:@hamatetsu_train
HP https://annakaworks.sakura.ne.jp/

