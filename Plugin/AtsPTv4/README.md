# Hamatetsu's BVE Trainsim Softwares
BVE Trainsim 6(一部はBVE Trainsim 5.8も)向けのATSプラグインです。

※現在はver4の機能改善、機能追加は行っていません。ver5をご利用ください。  

## 内容
・ATS-PTプラグイン  
・ATS-PTプラグイン設定ツール  
・BVEファイル編集ツール  
・Castもどきプラグイン  
・C-Staff Maker DXプラグイン  
  
  
## 構成
・ATS-PTプラグイン  
　｜※メインプラグイン  
　├ [AtsPT4s](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv4/AtsPT4s)　…ATS-PTプラグイン(カスタマイザブル版)  
　├ [AtsPT4](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv4/AtsPT4)　…ATS-PTプラグイン  
　├ [AtsPT4e](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv4/AtsPT4e)　…ATS-PTプラグイン(簡易版)  
　├ [AtsM](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv4/AtsM)　…M式ATS-PTプラグイン  
　├ [AtsMe](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv4/AtsMe)　…M式ATS-PTプラグイン(簡易版)  
　｜※サブプラグイン  
　├ [Chopper](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv4/Chopper)　…ブレーキチョッパプラグイン  
　├ [EbDevice](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv4/EBDevice)　…EB装置プラグイン  
　├ [Handle](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv4/Handle)　…ブレーキハンドルプラグイン  
　├ [HRTasc](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv4/HRTasc)　…TASCプラグイン  
　├ [MrDown](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv4/MRDown)　…MR圧調整プラグイン  
　├ [PlusBrake](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv4/PlusBrake)　…増圧ブレーキプラグイン  
　├ [PTHybrid](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv4/PTHybrid)　…ハイブリッドプラグイン  
　├ [Zihou2](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv4/Zihou2)　…時報プラグイン  
　｜※ATS-PTプラグインを他プラグイン向け路線で運転できるようにする中間プラグイン  
　├ [Metro2PT4](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv4/Metro2PT4)　　…メトロ総合プラグイン路線  
　├ [Swp2PT4](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv4/Swp2PT4)　…SWP2プラグイン路線  
　｜※ドキュメント類  
　└ Document  
  
・ATS-PTプラグイン設定ツール ver.1.0.25  
　└ [PTConfig](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/PTConfig)　…ATS-PTプラグインの設定編集ツール  
  
・車両パラメータ補助プラグイン  
　└ [AddPbTascPressure](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AddPbTascPressure)　…メインプラグイン、PlusBrake、HRTascプラグインのPressureRates設定補助プラグイン(要AtsEX)  
※ver5では自動で行うため、このプラグインは不要になります。  

・プラグインコンバータ  
└ Plugin  
　｜※メインプラグイン  
　├ [PluginConverter](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/PluginConverter)　…プラグインコンバーター  
　｜※ATS-PTプラグインを他プラグイン向け路線で運転できるようにする中間プラグイン  
　├ [Metro2PT4](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/Metro2PT4)　　…ATC信号値をATS-PTプラグイン向けに変換  
　├ [Swp2PT4](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/Swp2PT4)　…SWP2プラグインの地上子をATS-PTプラグイン向けに変換  
　｜※設定ツール  
　├ [PluginConverterConfig](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Tool/PluginConverterConfig)　…プラグインコンバータ設定ファイルの編集ツール  
　｜※ソースファイル  
　└ [PluginConverter](https://gitlab.com/hamatetsu_train/pluginconverter)  

・CASTもどき / Castもどきプラグイン ver.1.1.0  
└ Tool  
　├ [CastPanel](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Tool/CastPanelDX)　…CASTのような運転支援ツール  
　└ [CastPanelDX](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Tool/CastPanelDX)　…  車両データ組み込み用 & CASTもどきへの送信用
※2024年4月にCASTが更新されることになり、CastPanelも新しくなります。[CastPanel3](https://gitlab.com/hamatetsu_train/castpanel3)  
  
・名鉄タブレット時刻表もどき / 名鉄タブレット時刻表もどきプラグイン ver.1.0.0  
└ Tool  
　├ [MStaff](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Tool/MStaff)　…名鉄タブレット時刻表もどきアプリ  
　└ [MStaffEX](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Tool/MStaff)　…  車両データ組み込み用 & 名鉄タブレット時刻表もどきへの送信用
  
・C-Staff Maker DX ver.1.1.1  
└ Tool  
　└ [CStaffMakerDX](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Tool/CStaffMakerDX)　… ３の会社のようなスタフをパネルに組み込む 
  
  
## ドキュメント
　[ATS-PTプラグイン設定マニュアル](https://drive.google.com/file/d/19fsQSs_W-mGncqyesS-lPyLjEJdiumzE/view)  
　[ATS-PTプラグイン設定・インデックス表](https://docs.google.com/spreadsheets/d/14WknB_--XbCRWxe5jVMWrjsd73RDpNyQTDP6jYwwT2g/edit#gid=2134081760)(Google Spread Sheet)  
  
  
## 利用について
▼ダウンロードについて  
　利用したいプラグイン・ツールのあるフォルダに移動し、右上の↓マークを押し、ZIPを押してください。  
  このページの↓マークを押すと、すべてのプラグイン・ツールをダウンロードします。
  ダウンロードしたzipファイルを右クリック→プロパティをクリックし、「ブロックの解除」があったらチェックを付けてください。  
  その後、zipファイルを解凍してください。  
  
▼プラグインの利用について  
　[ガイドライン](https://annakaworks.sakura.ne.jp/20bvets/guideline.html)にしたがって利用することができます。  
  
  60fpsを超える環境でBVEをプレイした場合に、計算誤差が大きくなって意図しない動作となる疑惑があります。いちおう対策は入れてみましたが、60fps以下の環境でプレイすることをおすすめします。  
  →本件は、現在制作中のVer5にて解消予定です。  
  
  
▼プラグインを利用したデータの公開について  
　データに同梱のReadMe等に出展を明記した上で、運転可能な車両データと同梱する場合に限り、車両データ作者様による当プラグインの配布を認めます。  
　作者名はハルピー、ＵＲＬは[https://annakaworks.sakura.ne.jp/](https://annakaworks.sakura.ne.jp/)でお願いします。  
  
▼プラグインの解析について  
　プラグインを作成するにあたり仕様策定した際にご協力いただいた方からの要請に基づき、プラグインのデコンパイル等による解析は禁止しております。  
  

## こんなときは
▼「次のアプリケーションでは、Microsoft .NET Framework 4.8 またはそれ以降が必要です」のエラーが出る場合  
　このプラグインはBVE Trainsim 6と同じく.NET Flamework 4.8 向けで作成しています。そのため、.NET Flamework4.8がインストールされていない場合、上記のようなエラーが出る可能性があります。その場合は Windows Update 等で最新の.Net Flamework をインストールしてください。  
※Microsoft 以外のサイトからダウンロードしてインストールしたことにより損害が発生しても一切責任を負いません。自己責任でお願いします。  
※Windows 10 May 2019 Update (1903)以降のWindowsであれば入っているはず。  
  
▼「ネットワーク上の場所からアセンブリを読み込もうとしました。これにより、以前のバージョンの .NET Framework で、アセンブリがサンドボックス化された可能性があります。」みたいなエラーが出る場合  
　ダウンロードファイルに対するWindowsの保護機能が有効になっている可能性があります。dllファイルを右クリック→プロパティをクリックし、「ブロックの解除」があったらチェックを付けてください。  
  または、一旦削除した上で、「ダウンロードについて」の手順をやり直してください。  
  
▼最新版のBVE TrainsimでATS-PTプラグインVer.3が使えない  
　ATS-PTプラグインVer.3は、サポートを終了しました。  
　BVE Trainsim 5.8 / 6以降で動く他のプラグインから読み込んで使用しようとした場合も同様にサポート対象外です。  
  
▼走行中に故障ランプが点灯してATS-Pのブレーキがかかった  
　空転滑走検知により非常停止しました。  
　空転滑走していないのに発生した場合は、誤動作の可能性がありますので、以下の対策を行ってみてください。  
　(1) ver4ではなくver5を使用する。  
　(2) 高フレームレート環境でBVEをプレイしている場合は60fpsに抑える。  
　(3) 設定ファイルで空転滑走検知をオフにする。  
  
▼その他のエラーが出た場合  
　エラーを吐いたプラグイン名、プラグインのバージョン、エラーの内容、エラーが出た時の状況、操作、プラグイン設定ファイルの内容等を詳しく教えてください。  
　プラグインによっては同じフォルダにログファイルを出力していますので、そちらをお送りいただく場合があります。  
　「エラーが起きました！」とかBVEのエラーリストの羅列を送りつけてくるだけでは解決ができません。  
　また、エゴサしているわけでもありませんので、直接ご報告ください。  
  
  
