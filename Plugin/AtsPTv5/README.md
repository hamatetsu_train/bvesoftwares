# Hamatetsu's BVE Trainsim Softwares
BVE Trainsim 6(一部はBVE Trainsim 5.8も)向けのATSプラグインです。

※Windows11、Windows10以外での利用はサポート対象外となります。  
※BveEXを使用するため、別途BveEX(2.0.5以降)が必要です。  

## 対応機能
・保安装置  
　・ATS-PT/ST  
　・M式ATS/名市交CS-ATC
　・近鉄ATS (信号照査のみの簡易再現)  
　・(架空鉄道 浜松鉄道向け) ATS-PH  
・機能  
　・増圧ブレーキ  
　・TASC  
　・ATO  
　・架線電圧(ブレーキチョッパ、回生失効)  

## 構成
・ATS-PTプラグインv5  
　｜※メインプラグイン  
　├ [AtsPT5](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv5/AtsPT5)　…ATS-PTプラグイン  
　｜※ドキュメント類  
　└ Document  
  
・ATS-PTプラグイン設定ツール  
　└ [PTConfig5](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv5/PTConfig5)　…ATS-PTプラグインの設定編集ツール  
※AtsPT4の設定ファイルの読み込みも可能です。(ORPを使用する設定の場合に変な値になる可能性があります) 

  
## ドキュメント
・必要なファイル  
　・AtsPT5.dll：本体  
　・AtsPT5.json：設定ファイル  
　・Microsoft.Bcl.AsyncInterfaces.dll：AtsPT5.dllと同じフォルダに配置。  
　・System.Text.Encodings.Web.dll：AtsPT5.dllと同じフォルダに配置。  
　・System.Text.Json.dll：AtsPT5.dllと同じフォルダに配置。  
　・System.IO.Pipeline.dll：AtsPT5.dllと同じフォルダに配置。  
　　※これら4つを他のプラグインも使用する場合は、バージョンを合わせてください。    
※別途BveEXが必要です。　対応バージョン：2.0.5  
　本プラグインはAtsHandleのうち、Power、Brake、Reverser、ConstantSpeedを出力しています。  
※車両データに使用している専用プラグインも同梱しています。車両データのプラグインだけ差し替えたい場合に使用してください。  
  
  
・設定方法  
設定はjson形式の設定ファイルで行います。ファイル名はプラグインのファイル名と同じにしてください。  
※AtsPT5.dllに対しての設定ファイルはAtsPT5.json  
※配布する際は5つのDLLをすべて添付してください。VehiclePluginUsingへの指定はAtsPT5.dllのみ行ってください。  
　[ATS-PTプラグインv5設定](https://annakaworks.sakura.ne.jp/20bvets/pt/v5config.html)  
　[ATS-PTプラグイン設定マニュアル](https://drive.google.com/file/d/19fsQSs_W-mGncqyesS-lPyLjEJdiumzE/view)  
　[ATS-PTプラグイン設定・インデックス表](https://docs.google.com/spreadsheets/d/14WknB_--XbCRWxe5jVMWrjsd73RDpNyQTDP6jYwwT2g/edit#gid=2134081760)(Google Spread Sheet)  
  
  
## 利用について
▼ダウンロードについて  
　利用したいプラグイン・ツールのあるフォルダに移動し、右上の↓マークを押し、ZIPを押してください。  
  このページの↓マークを押すと、すべてのプラグイン・ツールをダウンロードします。
  ダウンロードしたzipファイルを右クリック→プロパティをクリックし、「ブロックの解除」があったらチェックを付けてください。  
  その後、zipファイルを解凍してください。  
  
▼プラグインの利用について  
　[ガイドライン](https://annakaworks.sakura.ne.jp/20bvets/guideline.html)にしたがって利用することができます。  
    
▼プラグインを利用したデータの公開について  
　データに同梱のReadMe等に出展を明記した上で、運転可能な車両データと同梱する場合に限り、車両データ作者様による当プラグインの配布を認めます。  
　作者名はハルピー、ＵＲＬは[https://annakaworks.sakura.ne.jp/](https://annakaworks.sakura.ne.jp/)でお願いします。  
  
▼プラグインの解析について  
　プラグインを作成するにあたり仕様策定した際にご協力いただいた方からの要請に基づき、プラグインのデコンパイル等による解析は禁止しております。  
  

## こんなときは
▼ Windows8.1以前、Windows以外で正常動作しない  
  MicrosoftがサポートしているWindows11、Windows10以外の環境での利用はサポート対象外です。  
  またWindows11であってもArm版はサポート対象外です。  

▼「次のアプリケーションでは、Microsoft .NET Framework 4.8 またはそれ以降が必要です」のエラーが出る場合  
　このプラグインはBVE Trainsim 6と同じく.NET Flamework 4.8 向けで作成しています。そのため、.NET Flamework4.8がインストールされていない場合、上記のようなエラーが出る可能性があります。その場合は Windows Update 等で最新の.Net Flamework をインストールしてください。  
※Microsoft 以外のサイトからダウンロードしてインストールしたことにより損害が発生しても一切責任を負いません。自己責任でお願いします。  
※Windows 10 May 2019 Update (1903)以降のWindowsであれば入っているはず。  
  
▼「ネットワーク上の場所からアセンブリを読み込もうとしました。これにより、以前のバージョンの .NET Framework で、アセンブリがサンドボックス化された可能性があります。」みたいなエラーが出る場合  
　ダウンロードファイルに対するWindowsの保護機能が有効になっている可能性があります。dllファイルを右クリック→プロパティをクリックし、「ブロックの解除」があったらチェックを付けてください。  
  または、一旦削除した上で、「ダウンロードについて」の手順をやり直してください。  
  
▼BveEXがらみのエラーが出る場合  
　[BveEXのサポートページ](https://www.okaoka-depot.com/AtsEX.Docs/support/)にてご確認ください。  
  
▼その他のエラーが出た場合  
　エラーを吐いたプラグイン名、プラグインのバージョン、エラーの内容、エラーが出た時の状況、操作、プラグイン設定ファイルの内容等を詳しく教えてください。  
　プラグインによっては同じフォルダにログファイルを出力していますので、そちらをお送りいただく場合があります。  
　「エラーが起きました！」とかBVEのエラーリストの羅列を送りつけてくるだけでは解決ができません。  
　また、エゴサしているわけでもありませんので、直接ご報告ください。  
  
  
