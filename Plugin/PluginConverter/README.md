# Hamatetsu's Bve Softwares - PluginConverter
BVE Trainsim 6 / 5.8向けのATSプラグインです。  
複数のプラグインを同時に使用したり、地上子情報を変換して指定したプラグインに渡すことができます。  
  
スクリプトはC++やC#は書けなくてもBVEのマップファイルの数式ぐらいなら書けるという方向けです。  
※C#が書けるなら普通にソースをいじったほうが効率がいいと思います…  
  
※2024/12/29 BveExにより、AtsEx.Caller.x～.dllが使用できなくなったため、本プラグインでBveExプラグインの指定ができなくなったおそれがあります。現状では回避策はありません。  
  
## 構成
├ [Debug] … デバッグ用(開発時の動作確認用で、配布禁止です。)  
｜ ├ [x64]  
｜ └ [x86]  
├ [x64] … BVE6向け  
├ [x86] … BVE5.8向け  
└ [設定ファイルサンプル]  
  
  
## ドキュメント
・設定方法  
設定はxml形式の設定ファイルで行います。ファイル名はプラグインコンバータのファイル名と同じにしてください。  
※PluginConverter.dllに対しての設定ファイルはPluginConverter.xml  
  
├ [DLL] … 読み込むDLLについての設定です。指定するDLLごとに記述します。  
｜　├ [Path] () … 読み込みたいDLLの、PluginConverterからの相対パス   
｜　├ [HasSetHandle] (false) … 読み込みたいDLLが SetHandle を持っているなら true (たぶんATS-PTプラグイン系のみ)   
｜　├ [HasGetVehicleState] (false) … 読み込みたいDLLが GetVehicleState を持っているなら true (たぶんまだ無い)   
｜　├ [InitializeNfbOn] (true) … シナリオ読み込み直後に起動していてほしいなら true  
｜　├ [Get] … 毎フレーム、前のプラグインの処理終了時の出力ハンドル値を受け取りたいならtrue (一番最初に指定したプラグインは無効)  
｜　｜　├ [Handle] (false) … Power・Brake・Reverser・Csc (SetHandleを持っている場合に限る)  
｜　｜　├ [Power] (true) … Power  
｜　｜　├ [Brake] (true) … Brake  
｜　｜　└ [Reverser] (true) … Reverser  
｜　├ [Set] … 毎フレーム、このプラグインの出力ハンドル値を反映させたいtrue  
｜　｜　├ [Power] (true) … Power・Brake・Reverser・Csc (SetHandleを持っている場合に限る)  
｜　｜　├ [Brake] (true) … Power・Brake・Reverser・Csc (SetHandleを持っている場合に限る)  
｜　｜　├ [Reverser] (true) … Power・Brake・Reverser・Csc (SetHandleを持っている場合に限る)  
｜　｜　└ [Csc] (true) … Power・Brake・Reverser・Csc (SetHandleを持っている場合に限る)  
｜　└ [KeyAssign] … 指定したキーを押すたびに起動状態のON・OFFを切り替えます。  
｜　　　├ [Input] (AtsKey) … キーの入力方法(AtsKeyかKeyCode)  
｜　　　├ [KeyCode] (-1) … キー(AtsKeyの場合:0～15、KeyCodeの場合:仮想ｷｰｺｰﾄﾞ、使用しない場合:-1)  
｜　　　└ [IsExclusive] (false) … このプラグインをONにした際に他のプラグインをすべてOFFにするなら true  
｜  
└ [GENERAL] … 変換スクリプトを記述します。  
　　├ [LogLevel] … ログの出力レベル(大きくなるほど細かく出ます) 0:控えめ 1:Info 2:Error 3:Debug  
　　├ [Beacon] … 地上子の変換スクリプトを記述します。(変換したいBeaconの数だけ記述)  
　　｜　├ [Type] (0) … 地上子番号(0以上)  
　　｜　└ [Script] () … スクリプト  
　　└ [Signal] … 信号インデックスの変換スクリプトを記述します。(変換したい信号インデックスの数だけ記述)※SetSignalイベントのときだけ発動  
　　　　├ [Index] (0) … 信号インデックス(0以上)  
　　　　└ [Script] () … スクリプト  
  
※( )内の値は記述しなかった場合の値
  
  
## スクリプトについて
▼記述について  
; または改行で区切ります。  
  
▼変数について  
数値を変数に記憶させることができます。変数に値を代入するには、変数、等号 ( = )、値または式の順に記述します。  
  
$foo = 1.067;  
  
引数やキーに変数を記述すると、変数に代入された値が参照されます。  
  
$foo2 = $foo; → $foo2 に 1.067 が代入されます。  
  
変数に代入できるのは、数値のみです。変数名の先頭には $ を付けます。変数名に使用できる文字は、英字 (A～Z、a～z)、数字 (0～9)です。  
値を代入する前に変数を呼び出した場合、0を返します。  
シナリオ読み込みもしくは駅ジャンプされるまで変数とその値を記憶しています。  
  
なお、内部的にはdouble型(小数有り数値)で保持するため、計算によっては多少の誤差が発生する場合があります。  
  
・指定変数
|変数|説明|
|----|----|
|distance|スクリプトが呼ばれた時点の距離程を返します。(先頭に$は不要)|
|$InType|スクリプトが呼ばれた時点の地上子番号を返します。|
|$InSig|スクリプトが呼ばれた時点の信号値を返します。|
|$InLen|スクリプトが呼ばれた時点の指定された閉塞までの距離を返します。|
|$InData|スクリプトが呼ばれた時点の送信データを返します。|
|$OutType|変換後の地上子番号をセットします。(未指定の場合は$InType、負の数をセットすると地上子をふまなかった扱いとなります)|
|$OutSig|変換後の信号値をセットします。(未指定の場合は$InSig)|
|$OutLen|変換後の指定された閉塞までの距離をセットします。(未指定の場合は$InLen)|
|$OutData|変換後の送信データをセットします。(未指定の場合は$InData)|
※信号インデックス変換の場合は、distance, $InSug, $OutSigのみ有効です  

  
▼数学関数について  
|関数|説明|
|----|----|
|abs(arg1)|arg1 の絶対値を返します。|
|atan2(arg1, arg2)|アークタンジェント(X軸正方向から原点～座標(arg2,arg1)の線分との角度)を返します。|
|ceil(arg1)|arg1 以上の最小の整数を返します (arg1 の切り上げ)。|
|cos(arg1)|arg1 の余弦 (コサイン) を返します。|
|equal(arg1, arg2)|arg1 = arg2 なら 1 を、異なるなら 0 を返します。|
|exp(arg1)|自然対数の底 (e) の arg1 乗を返します。|
|floor(arg1)|arg1 以下の最大の整数を返します (arg1 の切り捨て)。|
|if(arg1, arg2, arg3)|arg1 が 0でないなら arg2 を、arg1 が 0 なら arg3 を返します。|
|less(arg1, arg2)|arg1 < arg2 なら1を、arg1 >= arg2なら0を返します。|
|log(arg1)|arg1 の自然対数を返します。|
|max(arg1, arg2)|arg1 と arg2 の大きい方の値を返します。|
|min(arg1, arg2)|arg1 と arg2 の小さい方の値を返します。|
|more(arg1, arg2)|arg1 > arg2 なら1を、arg1 <= arg2なら0を返します。|
|pow(arg1, arg2)|arg1 の arg2 乗を返します。|
|sin(arg1)|arg1 の正弦 (サイン) を返します。|
|sqrt(arg1)|arg1 の平方根を返します。|
  
  
## 利用について
▼ダウンロードについて  
　利用したいプラグイン・ツールのあるフォルダに移動し、右上の↓マークを押し、ZIPを押してください。  
  このページの↓マークを押すと、すべてのプラグイン・ツールをダウンロードします。
  
▼プラグインの利用について  
　[ガイドライン](https://annakaworks.sakura.ne.jp/20bvets/guideline.html)にしたがって利用することができます。  
  
▼プラグインを利用したデータの公開について  
　データに同梱のReadMe等に出展を明記した上で、運転可能な車両データと同梱する場合に限り、車両データ作者様による当プラグインの配布を認めます。  
　作者名はハルピー、ＵＲＬは[https://annakaworks.sakura.ne.jp/](https://annakaworks.sakura.ne.jp/)でお願いします。  
  
  
## Ｑ＆Ａ
▼「64bitのプラグインと32bitのプラグインの混在はできるか？  
　できません。64bitのPluginConverterは64bitのプラグインのみ指定できます。32bitのPluginConverterは32bitのプラグインのみ指定できます。  
  
▼BveExを使用するプラグインを指定できるか？  
　現状ではできなくなったと思われます。  
  
▼AtsExを使用するプラグインを指定できるか？  
　できますが、一番最初に指定しないとAtsExがエラーを吐くようです。  
　また、AtsExを使用するプラグインを複数使用する場合は、それらをAtsEx.Callerのプラグイン指定の方にまとめていただき、PluginConverterにはAtsEx.Callerを1回だけ最初に指定してください。  
  
▼その他のエラーが出た場合  
　エラーを吐いたプラグイン名、プラグインのバージョン、エラーの内容、エラーが出た時の状況、操作、プラグイン設定ファイルの内容等を詳しく教えてください。  
　プラグインによっては同じフォルダにログファイルを出力していますので、そちらをお送りいただく場合があります。  
　「エラーが起きました！」とかBVEのエラーリストの羅列を送りつけてくるだけでは解決ができません。  
　また、エゴサしているわけでもありませんので、直接ご報告ください。  
  
  
##  
　このプラグインはRockOn様の[BveAtsPluginCsharpTemplate](https://github.com/mikangogo/BveAtsPluginCsharpTemplate)を使用しています。  

※ソースファイル  
　└ [PluginConverter](https://gitlab.com/hamatetsu_train/pluginconverter)  
