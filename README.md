# Hamatetsu's BVE Trainsim Softwares
BVE Trainsim 6(一部はBVE Trainsim 5.8も)向けのATSプラグインです。


## 構成
└ [Plugin](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin)  
｜├ [AtsPT v5](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv5)　…ATS-PTプラグイン系列 ver5(要AtsEX)  
｜｜└ [PTConfig5](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv5/PTConfig5)　…ATS-PTプラグイン系列 ver5 設定ツール  
｜├ [AtsPT v4](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/AtsPTv4)　…ATS-PTプラグイン系列 ver4  
｜｜└ [PTConfig4](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/PTConfig4)　…ATS-PTプラグイン系列 ver4 設定ツール  
｜├ [HROneman](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/HROneman)　…ローカルワンマンプラグイン(要AtsEX)  
｜├ [PluginConverter](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Plugin/PluginConverter)　…プラグインコンバーター  
｜｜※ドキュメント類  
｜└ Document  
｜  
└ Tool  
　｜・BVEファイル編集ツール   
　├ [BveEditor](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Tool/BveEditor)　…BVEのシナリオ・車両・運転台パネルの編集ツール  
　├ [BveScenarioSearch](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Tool/BveScenarioSearch)　…BVEのシナリオ・車両を選択してBVEを起動するツール  
　｜  
　｜・CASTもどき / Castもどきプラグイン   
　├ [CastPanel](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Tool/CastPanel)　…CASTのような運転支援ツールおよびプラグイン  
　｜  
　｜・名鉄タブレット時刻表もどき / 名鉄タブレット時刻表もどきプラグイン   
　├ [MStaff](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Tool/MStaff)　…名鉄タブレット時刻表もどきアプリおよびプラグイン  
　｜  
　｜・プラグインコンバータ  
　｜※設定ツール  
　└ [PluginConverterConfig](https://gitlab.com/hamatetsu_train/bvesoftwares/-/tree/main/Tool/PluginConverterConfig)　…プラグインコンバータ設定ファイルの編集ツール  

・その他(外部サイト)  
　├ [ImageToDDS](https://hamatetsu.booth.pm/items/1644850)　…指定したフォルダの画像ファイルをDDS形式に変換。xファイルも書き換え。変換後のフォルダをzipで固めることもできます。  


  
## 利用について
▼ダウンロードについて  
　利用したいプラグイン・ツールのあるフォルダに移動し、右上の↓マークを押し、ZIPを押してください。  
  このページの↓マークを押すと、すべてのプラグイン・ツールをダウンロードします。
  ダウンロードしたzipファイルを右クリック→プロパティをクリックし、「ブロックの解除」があったらチェックを付けてください。  
  その後、zipファイルを解凍してください。  
  
▼プラグインの利用について  
　[ガイドライン](https://annakaworks.sakura.ne.jp/20bvets/guideline.html)にしたがって利用することができます。  
  
▼プラグインを利用したデータの公開について  
　データに同梱のReadMe等に出展を明記した上で、運転可能な車両データと同梱する場合に限り、車両データ作者様による当プラグインの配布を認めます。  
　作者名はハルピー、ＵＲＬは[https://annakaworks.sakura.ne.jp/](https://annakaworks.sakura.ne.jp/)でお願いします。  
  

## こんなときは
▼ Windows8.1以前、Windows以外で正常動作しない  
  MicrosoftがサポートしているWindows11、Windows10以外の環境での利用はサポート対象外です。  
  またWindows11であってもArm版はサポート対象外です。  

▼「次のアプリケーションでは、Microsoft .NET Framework 4.8 またはそれ以降が必要です」のエラーが出る場合  
　このプラグインはBVE Trainsim 6と同じく.NET Flamework 4.8 向けで作成しています。そのため、.NET Flamework4.8がインストールされていない場合、上記のようなエラーが出る可能性があります。その場合は Windows Update 等で最新の.Net Flamework をインストールしてください。  
※Microsoft 以外のサイトからダウンロードしてインストールしたことにより損害が発生しても一切責任を負いません。自己責任でお願いします。  
※Windows 10 May 2019 Update (1903)以降のWindowsであれば入っているはず。  
  
▼「ネットワーク上の場所からアセンブリを読み込もうとしました。これにより、以前のバージョンの .NET Framework で、アセンブリがサンドボックス化された可能性があります。」みたいなエラーが出る場合  
　ダウンロードファイルに対するWindowsの保護機能が有効になっている可能性があります。dllファイルを右クリック→プロパティをクリックし、「ブロックの解除」があったらチェックを付けてください。  
  または、一旦削除した上で、「ダウンロードについて」の手順をやり直してください。  
  
▼わからないエラーが出た場合  
　エラーを吐いたプラグイン名、プラグインのバージョン、エラーの内容、エラーが出た時の状況、操作、プラグイン設定ファイルの内容等を詳しく教えてください。  
　プラグインによっては同じフォルダにログファイルを出力していますので、そちらをお送りいただく場合があります。  
　「エラーが起きました！」とかBVEのエラーリストの羅列を送りつけてくるだけでは解決ができません。  
　また、エゴサしているわけでもありませんので、直接ご報告ください。  
  
  
